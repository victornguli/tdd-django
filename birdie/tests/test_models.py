import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestPost:
    @pytest.mark.model_test
    def test_model(self):
        obj = mixer.blend("birdie.Post")
        assert obj.pk == 1, "Should create a Post Instance"

    @pytest.mark.model_test
    def test_excerpt(self):
        obj = mixer.blend("birdie.POST", body="Hello, World")
        result = obj.get_excerpt(5)
        assert result == "Hello", "Should return first 5 characters"
