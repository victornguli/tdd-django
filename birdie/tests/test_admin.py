from .. import admin
from .. import models

import pytest
from mixer.backend.django import mixer
from django.contrib.admin.sites import AdminSite
pytestmark = pytest.mark.django_db


class TestPostAdmin:
    def test_excerpt(self):
        site = AdminSite()
        post_admin = admin.PostAdmin(models.Post, site)

        obj = mixer.blend("birdie.POST", body="Hello, World")
        result = post_admin.excerpt(obj)
        assert result == "Hello", "Should return first few characters"

    def test_body_in_admin(self):
        site = AdminSite()
        post_admin = admin.PostAdmin(models.Post, site)

        obj = mixer.blend("birdie.POST", body="Hello Test")
        result = post_admin.body_text(obj)
        assert result == "Hello Test", "Should return all passed characters"
