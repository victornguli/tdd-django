from .. import views
from django.test import RequestFactory
from django.contrib.auth.models import AnonymousUser
from django.http import Http404
import pytest
from mixer.backend.django import mixer
from django.core import mail
from mock import patch
pytestmark = pytest.mark.django_db


@pytest.fixture
def get_req():
    req = RequestFactory().get("/")
    return req


class TestHomeView:
    """
    Tests A Class based view (HomeView).
    """
    @pytest.mark.view_test
    def test_anonymous(self, get_req):
        # req = RequestFactory().get("/")
        resp = views.HomeView.as_view()(get_req)
        assert resp.status_code == 200, "Should be callable by anyone"


class TestIndexView:
    """
    Tests A function based view that only returns an HttpResponse text.
    """
    @pytest.mark.view_test
    def test_anonymous(self, get_req):
        # req = RequestFactory().get("/")
        resp = views.index_view(get_req)
        assert resp.status_code == 200, "Should be callable by anyone"


class TestAdminView:
    """
    Tests A Login required view that is only accessible to authenticated users.
    """
    @pytest.mark.view_test
    def test_anonymous(self, get_req):
        # req = RequestFactory().get("/")
        get_req.user = AnonymousUser()
        resp = views.AdminView.as_view()(get_req)
        assert 'login' in resp.url, 'Should redirect to login'

    @pytest.mark.view_test
    def test_authenticated(self, get_req):
        user = mixer.blend("auth.User", is_superuser=False)
        # req = RequestFactory().get("/")
        get_req.user = user
        resp = views.AdminView.as_view()(get_req)
        assert resp.status_code == 200, "Should be accessible by a superuser"


class TestPostUpdateView:
    @pytest.mark.view_test
    def test_get(self, get_req):
        # req = RequestFactory().get("/")
        get_req.user = AnonymousUser()
        obj = mixer.blend("birdie.POST")
        res = views.PostUpdateView.as_view()(get_req, pk=obj.pk)
        assert res.status_code == 200, "Should be callable by anyone"

    @pytest.mark.view_test
    def test_post(self):
        post = mixer.blend("birdie.POST")
        data = {"body": "Another Post Body"}
        req = RequestFactory().post("/", data=data)
        req.user = AnonymousUser()
        res = views.PostUpdateView.as_view()(req, pk=post.pk)
        assert res.status_code == 302, "Should redirect to success view"
        post.refresh_from_db()
        assert post.body == "Another Post Body", "Should Update the post"

    @pytest.mark.view_test
    def test_security(self):
        user = mixer.blend("auth.User", first_name="Victor")
        post = mixer.blend("birdie.Post")
        req = RequestFactory().post("/", data={})
        req.user = user
        with pytest.raises(Http404):
            views.PostUpdateView.as_view()(req, pk=post.pk)


class TestPaymentView:
    @pytest.mark.view_test
    @patch("birdie.views.stripe")
    def test_payent(self, mock_stripe):
        mock_stripe.Charge.return_value = {"id": "234"}
        req = RequestFactory().post("/", data={"token": "123"})
        resp = views.PaymentView.as_view()(req)
        assert resp.status_code == 302, "Should Redirect to success page"
        assert len(mail.outbox) == 1, "Should send mail"
