from .. import forms
import pytest
pytestmark = pytest.mark.django_db


class TestPostForm:
    @pytest.mark.form_test
    def test_form(self):
        form = forms.PostForm(data={"body": ""})
        assert form.is_valid() is False, "Should be invalid if no data is given"

        form = forms.PostForm(data={"body": "Hello"})
        assert form.is_valid() is False, "Should be invalid if data is too short"
        assert "body" in form.errors, "Should have body field error"

        form = forms.PostForm(data={"body": "Hello, World !!!!!!!!!!!! more than 10 chars"})
        assert form.is_valid() is True, "Should be valid if data is long enough"
