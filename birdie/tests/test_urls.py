from .. import views
from django.test import RequestFactory
import pytest


class TestUrls:
    def test_index_view_url(self):
        req = RequestFactory().get("birdie/")
        resp = views.index_view(req)

        assert resp.status_code == 200, "The url birdie/ should resolve to the index view"
