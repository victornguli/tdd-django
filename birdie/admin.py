# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from . import models


class PostAdmin(admin.ModelAdmin):
    model = models.Post
    list = ("excerpt", "body_text",)

    def excerpt(self, obj):
        return obj.get_excerpt(5)

    def body_text(self, obj):
        return obj.body


admin.site.register(models.Post, PostAdmin)

